# MyRobot_Solutis

Código do robô criado com o robocode na linguagem Java para o desafio "Coding a Robot" do evento #Quero ser DEV! Talent Sprint Solutis.



# Por que o nome é Laxus?
Como ele possui uma movimentação "rápida" e eu estava meio que inspirado, resolvi por o nome de um personagem do anime Fairy Tail por gostar bastante.

# Pontos fortes
Seu maior ponto forte é a movimentação irregular, a ideia é que ele foque em um alvo por vez e nunca pare de se movimentar para os lados com intuito de dificultar receber tiros. Porém quando recebe dano, a movimentação dele graças a função onHitByBullet altera um pouco a movimentação, ou seja torna ela um pouco irregular.


# Pontos Fracos
Acredito que o maior ponto fraco seria contra robôs que prendem o outro na parede, ele trava/fica preso uns segundos ao não conseguir se mover na parede.

# O que eu achei desta oportunidade?
Particularmente gostei bastante de aprender um pouco sobre construir robôs em forma de tanque para acabar com outros robôs, assim como diz na documentação é divertido. Não tinha pensado que com conhecimentos de java era possivel fazer algo tão massa.


# Onde está o robô?
Ele está em robocode > robots > myRobots > laxus.java

Ou seja, dentro da pasta robocode vá até a pasta robots, em seguida a pasta myRobotos e lá estará o arquivo principal. Porém existem classes como enemyBot e advancedEnemyBot que são extrememante necessárias para funcionar.

# Base
Todo o código foi baseado em uma serie de posts de Robocode Lessons que vão de #1 até #5 e umas mudanças pessoais baseadas em outro robô.







<br>
<br>
<br>
<br>
Created by: Lucas Rosa