package laxus;
import robocode.*;
import java.awt.Color;
import java.awt.geom.Point2D;



// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Laxus - a robot by (your name here)
 */


public class Laxus extends AdvancedRobot {
    /**
     * run: Laxus's default behavior
     */

    private AdvancedEnemyBot enemy = new AdvancedEnemyBot();
    private byte scanDirection = 1;
    private byte moveDirection = 1;
	private int  moveRightValue = 90;


    public void run() {
       

        setColors(Color.yellow, Color.black, Color.red); 

        setAdjustRadarForRobotTurn(true);
        setAdjustRadarForGunTurn(true);
		setAdjustGunForRobotTurn(true);
		

		

        while (true) {

            radar();
			move();
			gun();
			execute();

        }
    }

     // onScannedRobot: Função que define oque acontece quando tu encontra outros robôs
    public void onScannedRobot(ScannedRobotEvent e) {
       
        if (enemy.none() || e.getDistance() < enemy.getDistance() - 70 ||
            e.getName().equals(enemy.getName())) {

            // track him using the NEW update method
            enemy.update(e, this);
        }

    }

    
     // onHitByBullet: Função que define oque acontece quando tu é atingido
	public void onHitByBullet(HitByBulletEvent e) {
	moveRightValue*= (90 * moveDirection);

}
	 
	 
    // onHitWall: Função que define oque acontece quando tu bate na parede ou em algo
    public void onHitWall(HitWallEvent e) {
       		 out.println("OUCH! I hit a wall anyway!"); 
			 	moveRightValue*= (90 * moveDirection);

    }

    // onHitRobot: Função que define oque acontece quando tu acerta um robô
    public void onHitRobot(HitRobotEvent e) {
     	moveRightValue = moveRightValue * 2; 

    }

    // Função que define o funcionamento do radar
    public void radar() {
        if (enemy.none()) {
            // Verifica os arredores caso não tenha nenhum inimigo
            setTurnRadarRight(360);
        } else {
            // oscila o radar
            double oscilate = getHeading() - getRadarHeading() + enemy.getBearing();
            oscilate += 30 * scanDirection;
            setTurnRadarRight(normalizeBearing(oscilate));
            scanDirection *= -1;
        }
    }
    
    
    public void move() {

        // Sempre muda para a direção do inimigo
		setTurnRight(normalizeBearing(enemy.getBearing() + moveRightValue - (15 * moveDirection)));

    
        if (getTime() % 20 == 0) {
            moveDirection *= -1;
            setAhead(150 * moveDirection);
        }
    }
	
        void gun() {

		// Não atira se não tiver um inimigo
		if (enemy.none()) {
			return;
		}
		// Calcula o poder de fogo do tiro baseado na distância
		double firePower = Math.min(400 / enemy.getDistance(), 3);
		// Calcula a velocidade da bala
		double bulletSpeed = 20 - firePower * 3;
		// distancia = rate * time
		long time = (long)(enemy.getDistance() / bulletSpeed);

		// Calcula o giro da arma para a localização x e y
		double futureX = enemy.getFutureX(time);
		double futureY = enemy.getFutureY(time);
		double absDeg = absoluteBearing(getX(), getY(), futureX, futureY);

		// Gura a arma para a direção x e y
		setTurnGunRight(normalizeBearing(absDeg - getGunHeading()));

		// se a arma estiver fria e estivermos apontados na direção certa, atire.
		if (getGunHeat() == 0 && Math.abs(getGunTurnRemaining()) < 10) {
			setFire(firePower);
		}
	}


    public void onRobotDeath(RobotDeathEvent e) {
        // Verificar se o robô que estavamos focando morreu
        if (e.getName().equals(enemy.getName())) {
            enemy.reset();
        }
    }


    // Normaliza o bearing entre +180 e -180
    double normalizeBearing(double angle) {
        while (angle > 180) angle -= 360;
        while (angle < -180) angle += 360;
        return angle;
    }



    double absoluteBearing(double x1, double y1, double x2, double y2) {
        double xo = x2 - x1;
        double yo = y2 - y1;
        double hyp = Point2D.distance(x1, y1, x2, y2);
        double arcSin = Math.toDegrees(Math.asin(xo / hyp));
        double bearing = 0;

        if (xo > 0 && yo > 0) { // both pos: lower-Left
            bearing = arcSin;
        } else if (xo < 0 && yo > 0) { // x neg, y pos: lower-right
            bearing = 360 + arcSin; // arcsin is negative here, actuall 360 - ang
        } else if (xo > 0 && yo < 0) { // x pos, y neg: upper-left
            bearing = 180 - arcSin;
        } else if (xo < 0 && yo < 0) { // both neg: upper-right
            bearing = 180 - arcSin; // arcsin is negative here, actually 180 + ang
        }

        return bearing;
    }


}
